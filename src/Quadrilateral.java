//class that represents Quadrilateral
public class Quadrilateral extends GeometricFigures{
    Point[] cornerPoints;

    Quadrilateral (Point[] cornerPoints) {
        this.cornerPoints = cornerPoints;
    }

    //we override parent class's abstract method
    @Override
    public Point[] getPoints() {
        return cornerPoints;
    }

    //we override interface's method
    // quadrilateral: area = side * other side
    @Override
    public double area() {
        int[] bounds = calculateBoundaries();

        assert bounds[0] >= bounds[2];
        assert bounds[1] >= bounds[3];

        return (bounds[0] - bounds[2]) * (bounds[1] - bounds[3]);
    }
}
