import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        List<GeometricFigures> figures = new ArrayList<>();

        //decided to use switch/case to handle process of creating shapes and receiving their boundaries and area
        //added them right after the creation of figure just to make the code lighter
        while (true) {
            System.out.println("A circle is defined by a centre and a perimeter point, the others by corner points");
            System.out.println("Choose a shape to create:");
            System.out.println("1. Circle");
            System.out.println("2. Triangle");
            System.out.println("3. Quadrilateral");
            System.out.println("4. Exit choosing and calculate");

            int choice = reader.nextInt();

            if (choice == 4) {
                break;
            }

            Point[] points = null;

            switch (choice) {
                case 1 -> {
                    System.out.println("Enter 2 points for the Circle:");
                    points = readPoints(reader, 2);
                    Circle circle = new Circle(points);
                    figures.add(circle);
                }
                case 2 -> {
                    System.out.println("Enter 3 points for the Triangle:");
                    points = readPoints(reader, 3);
                    Triangle triangle = new Triangle(points);
                    figures.add(triangle);
                }
                case 3 -> {
                    System.out.println("Enter 4 points for the Quadrilateral:");
                    points = readPoints(reader, 4);
                    Quadrilateral quadrilateral = new Quadrilateral(points);
                    figures.add(quadrilateral);
                }
                default -> System.out.println("Invalid choice. Try again.");
            }
        }

        if(figures.size()>1) {
            calculateAndShowTotal(figures);
        } else {
            for(GeometricFigures f:figures) {
                showBoundaries(f);
                showArea(f);
            }
        }


        reader.close();
    }

    //by asking a number of points of figure we can use multiple different shapes in the future
    private static Point[] readPoints (Scanner reader,int numPoints){
        Point[] points = new Point[numPoints];
        for (int i = 0; i < numPoints; i++) {
            System.out.println("Enter x and y coordinates for point " + (i + 1) + ":");
            int x = reader.nextInt();
            int y = reader.nextInt();
            points[i] = new Point(x, y);
        }
        return points;
    }

    //here we are using polymorphism to call method boundaries even when we don't know what kind
    //of figure will be passed to method using common parent class
    private static void showBoundaries (GeometricFigures figure) {
        System.out.println("Boundaries:");
        for (Point point : figure.boundaries()) {
            System.out.println("Point: (" + point.x() + ", " + point.y() + ")");
        }
    }
    //here we are doing the same for area method, which is specific for each figure
    private static void showArea (GeometricFigures figure) {
         System.out.println("Area: " + figure.area());
    }

    private static void calculateAndShowTotal(List<GeometricFigures> figures) {
        double totalArea = 0;
        int xMin = Integer.MAX_VALUE, yMin = Integer.MAX_VALUE;
        int xMax = Integer.MIN_VALUE, yMax = Integer.MIN_VALUE;

        for (GeometricFigures figure : figures) {
            totalArea += figure.area();
            Point[] bounds = figure.boundaries();

            xMin = Math.min(xMin, bounds[1].x());
            yMin = Math.min(yMin, bounds[1].y());
            xMax = Math.max(xMax, bounds[0].x());
            yMax = Math.max(yMax, bounds[0].y());
        }

        System.out.println("Total Area: " + totalArea);
        System.out.println("Combined Boundaries: ");
        System.out.println("Top-Right: (" + xMax + ", " + yMax + ")");
        System.out.println("Bottom-Left: (" + xMin + ", " + yMin + ")");
    }
}
