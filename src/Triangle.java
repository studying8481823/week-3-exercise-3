//class that represents Triangle
public class Triangle extends GeometricFigures{
    Point[] cornerPoints;

    Triangle (Point[] cornerPoints) {
        this.cornerPoints = cornerPoints;
    }

    //we override parent class's abstract method
    @Override
    public Point[] getPoints() {
        return cornerPoints;
    }

    //we override interface's method
    // triangle: draw a quadrilateral around it and calculate half of the area
    @Override
    public double area() {
        int[] bounds = calculateBoundaries();

        assert bounds[0] >= bounds[2];
        assert bounds[1] >= bounds[3];

        return (bounds[0] - bounds[2]) * (bounds[1] - bounds[3]) / 2;
    }
}
