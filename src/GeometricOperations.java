//interface which determines which operations are we going to use for our figures
public interface GeometricOperations {
    Point[] boundaries();
    double area();
}
