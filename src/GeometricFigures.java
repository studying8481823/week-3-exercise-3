//made and abstract parent class for all our current or future shapes and to implement intarface for
//all child classes
//abstract, because we don't need to make any instances of this class in the future
//all
abstract class GeometricFigures implements GeometricOperations {
    //method that will return array with points of child classes figure
    abstract Point[] getPoints();

    //decided to split method from the given example for clearer understanding
    protected int[] calculateBoundaries() {
        int x1 = Integer.MIN_VALUE;
        int y1 = Integer.MIN_VALUE;
        int x2 = Integer.MAX_VALUE;
        int y2 = Integer.MAX_VALUE;

        for (Point point : getPoints()) {
            x1 = Math.max(x1, point.x());
            y1 = Math.max(y1, point.y());
            x2 = Math.min(x2, point.x());
            y2 = Math.min(y2, point.y());
        }

        return new int[]{x1, y1, x2, y2};
    }

    @Override
    public Point[] boundaries() {
        int[] bounds = calculateBoundaries();

        assert bounds[0] >= bounds[2];
        assert bounds[1] >= bounds[3];

        return new Point[]{new Point(bounds[0], bounds[1]), new Point(bounds[2], bounds[3])};
    }
}
