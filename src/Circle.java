//class that represents Circle
public class Circle extends GeometricFigures{

    Point[] centreAndPerimeterPoints;

    Circle (Point[] centreAndPerimeterPoints) {
        this.centreAndPerimeterPoints = centreAndPerimeterPoints;
    }

    //we override parent class's abstract method
    @Override
    public Point[] getPoints() {
        return centreAndPerimeterPoints;
    }

    //we override interface's method
    // circle: area = pi * r^2 ja r = sqrt(dx^2 + dy^2), so area = pi * (dx^2 + dy^2)
    @Override
    public double area() {
        int [] bounds = calculateBoundaries();

        assert bounds[0] >= bounds[2];
        assert bounds[1] >= bounds[3];

        return Math.PI * ((bounds[0] - bounds[2]) * (bounds[0] - bounds[2]) + (bounds[1] - bounds[3]) * (bounds[1] - bounds[3]));
    }
}
