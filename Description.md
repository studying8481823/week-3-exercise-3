Our implementation is quite messy and difficult to maintain. It is suspected that it could benefit more from reuse. The desired features are:

The program could potentially support more shapes in the future than the three currently listed (triangle, quadrilateral, circle).
More calculable operations should be supported than currently (area and boundaries).
The number of shapes could also vary from zero to an arbitrarily large number.
One generally desired feature is that the program can better utilize the support offered by the compiler and the language types for describing the operational logic, given that a compiled language is being used (Java, but also applicable to other languages).
Task: Design a structure and implementation for the given program that utilizes object-oriented programming principles so that the desired features are better achievable. The result should be the whole package in such a way that a sensible solution principle has been found for the aforementioned points, which can be explained and seems maintainable. Justify the choices you made and explain how the solution would be used.

Tip: The part 3 of the study material includes the topics polymorphism and inheritance. It is possibly desired that these be utilized in the response. The end result should also include implementations of the solutions, but you do not need to reinvent the implementations, as they are already provided as part of the existing code e.g. feel free to copy code in from this exercise's description.